#ifndef __DTSerialProxy__H__
#define __DTSerialProxy__H__

#include <fcntl.h> // Contains file controls like O_RDWR
#include <errno.h> // Error integer and strerror() function
#include <termios.h> // Contains POSIX terminal control definitions
#include <unistd.h> // write(), read(), close()
#include <cstring> // strncpy(), memset()
#include <cstdio> // printf()
#include <iostream>
#include <cstdint>
#include <stdint.h>

#include "fake_visa.h"

class DTSerialProxy
{  
 public:
  
  DTSerialProxy();
  ~DTSerialProxy();
  
  int fd;
  struct termios tty;
  fd_set fdSet;
  struct timeval timeout;
  ViChar termchar = '\n';
  bool termcharEnabled = true;
  
  
  // Various arguments to the following methods are intentionally unused so that the interface is the same as NI-VISA's.
  // mode and timeout intentionally unused
  ViSession viOpen(ViConstRsrc name, ViAccessMode mode, ViUInt32 timeout);
  inline ViStatus viOpenDefaultRM () { return 0; }
  inline ViStatus viClose() { return close(fd); }
  
  // retCnt intentionally unused
  ViStatus viWrite(ViBuf buf, ViUInt32 cnt, ViPUInt32 retCnt);
  ViStatus viRead(ViBuf buf, ViUInt32 cnt, ViPUInt32 retCnt);
  ViStatus viSetAttribute(ViAttr attrName, ViAttrState attrValue);
  
  // status intentionally unused
  inline ViStatus viStatusDesc(ViStatus status, ViChar desc[]) 
  {
    // NI defines buffer as at least 256
    strncpy(desc, "TSerialProxy: Fake NI-VISA does not support error description", 200);
      return 0;
  };
  
 private:
  /* Delete copy constructor and assignment operator */
  DTSerialProxy (const DTSerialProxy &);
  DTSerialProxy & operator= (const DTSerialProxy & other);
  
};

#endif
