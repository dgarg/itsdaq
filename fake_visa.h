/**
 * @file serial/fake_visa/fake_visa.h
 * @brief Dummy NI-VISA types and enums for use with TSerialProxy
 *
 * This file redefines various types and constants from NI-VISA's visa.h and visatype.h.
 * It exists so that code written for the TSerialProxy class can run even if ITSDAQ was compiled without NI-VISA
 * support. This is done in the CI environment (which can't possibly test VISA without any hardware) and can also be
 * done in normal Linux builds of ITSDAQ.
 *
 * When ITSDAQ is compiled without NI-VISA support, the TSerialProxy class must present exactly the same NI-VISA-like
 * interface that it would if NI-VISA were enabled. This requires a subset of types and constants from the real visa.h
 * to be defined.
 */
#ifndef FAKE_VISA_H
#define FAKE_VISA_H
#ifndef __VISA_HEADER__ // Ensure that the real visa.h hasn't been included, to avoid a conflict with its types
#include <cstdint>

typedef int32_t ViStatus;
typedef uint32_t ViUInt32;
typedef ViUInt32 ViSession;
typedef ViUInt32 ViAttr;
typedef ViUInt32 ViAccessMode;
typedef ViUInt32* ViPUInt32;

// This essentially replicates the behaviour of the real NI-VISA header.
#if INTPTR_MAX == INT64_MAX // 64-bit system
      typedef uint64_t ViAttrState;
#else // 32-bit system
      typedef ViUInt32 ViAttrState;
#endif

typedef char ViChar;
typedef unsigned char* ViBuf;
typedef char* ViRsrc;
typedef const ViRsrc ViConstRsrc;

constexpr ViUInt32 VI_NULL = 0;
constexpr ViAttrState VI_TMO_IMMEDIATE = 0;
constexpr ViAttrState VI_TMO_INFINITE = INT32_MAX;

// All fails are negative for quick check
enum {
    VI_SUCCESS = 0,
    VI_SUCCESS_TERM_CHAR = 1,
    VI_SUCCESS_MAX_CNT = 2,
    VI_ERROR_TMO = -2,
    VI_ERROR_IO = -1,
    VI_ERROR_NSUP_ATTR_STATE = -3,
};

enum {
    VI_ATTR_TMO_VALUE = 0x100,
    VI_ATTR_ASRL_BAUD,
    VI_ATTR_ASRL_DATA_BITS,
    VI_ATTR_ASRL_DTR_STATE,
    VI_ATTR_ASRL_FLOW_CNTRL,
    VI_ATTR_ASRL_PARITY,
    VI_ATTR_ASRL_RTS_STATE,
    VI_ATTR_ASRL_STOP_BITS,
    VI_ATTR_ASRL_END_IN,
    VI_ATTR_ASRL_END_OUT,
    VI_ATTR_TERMCHAR,
    VI_ATTR_TERMCHAR_EN,

    VI_ASRL_END_NONE,
    VI_ASRL_END_TERMCHAR,

    VI_ASRL_PAR_NONE,
    VI_ASRL_PAR_EVEN,
    VI_ASRL_PAR_ODD,

    VI_ASRL_STOP_ONE,
    VI_ASRL_STOP_TWO,

    VI_ASRL_FLOW_NONE,
    VI_ASRL_FLOW_XON_XOFF,
    VI_ASRL_FLOW_RTS_CTS,
};

inline ViSession viOpen(ViSession, void*, ViUInt32, ViUInt32, void*) { return VI_SUCCESS; }
inline ViStatus viClose(ViSession) { return VI_ERROR_IO; }
inline ViStatus viOpenDefaultRM(void*) { return VI_SUCCESS; }
inline ViStatus viWrite(ViSession, ViBuf, ViUInt32, ViUInt32* retCnt) {
    retCnt = nullptr;
    return VI_ERROR_IO;
}
inline ViStatus viRead(ViSession, ViBuf, int, ViUInt32* retCnt) {
    retCnt = nullptr;
    return VI_ERROR_IO;
}
inline ViStatus viSetAttribute(ViSession, int, int) { return -1; }
inline ViStatus viStatusDesc(ViSession, ViStatus, ViChar desc[]) {
    desc[0] = '\0';
    return VI_ERROR_IO;
}
#endif // !__VISA_HEADER__
#endif // !FAKE_VISA_H
