#ifndef TTi_h
#define TTi_h

#include <stdlib.h>

// windows serial model is _very_ different, Carlos!
#include <termios.h>
#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <fcntl.h> 
#include <string.h>
#include <cstdint>
#define TRUE 1

//#include "../../dcsdll/TDCSDevice.h"

enum LvDeviceList{
  LV_GW_PST3202GP,
};

class TTiPrivate;

class TTi /*:public TDCSDevice*/ {
 public:
  
  TTi(char* resName);
  TTi(char* resName, int channel, char* label);
  TTi(char* resName, int channel, char* label, float maxVoltage, float maxCurrent);
  TTi(unsigned int gpib);
  TTi(unsigned int gpib, int channel, char* label);
  TTi(unsigned int gpib, int channel, char* label, float maxVoltage, float maxCurrent);
  ~TTi();

  void initSupply(char* resName, int channel, char* label); 

  /*bool Mon();
  bool Mon(unsigned char* status, float* V, float* I);
  */
  bool Off();
  bool On();
  bool isValid;
  //bool PrintStatus();
  /*bool PrintOutput();

  bool ReadLabel(char* label);
  bool ReadLimits(float* V, float* I);
  bool ReadSoftLimits(float* V, float* I);

  bool ReadSettings();
  bool ReadSettings(float* V, float* I);

  bool Set(float v, float i);
  bool Set(bool on, bool rc, float v, float i);
  bool SetSoftLimits(float v, float i);
  */
  char model[64];
  char sn[64];
  
 private:

  // Don't allow settings above these, even if PS capable of it
  float vMaxSoft, iMaxSoft;
  // During startupa, don't use the limits if they're not specified
  bool softLimitsSet;

  float vMax, iMax, vMon, iMon, vSet, iSet;
  unsigned char data[1024];
  char stringinput[128];
  char m_resource[256];
  char m_outLabel[512];
  int  m_outChannel;
  int m_deviceType;
  bool debug;

  // Mostly access to serial port, hidden from CINT/CLING
  // The comment on the following line suppresses an incomplete type warning.
  TTiPrivate *port; //!

  //  ClassDef(TTi,1);

};

#endif
