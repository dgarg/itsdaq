#include "TTi.h"
//#include "../../stlib/st_error.cpp"

#include <string>
#include <iostream>

//#include <Riostream.h>

#include "DTSerialProxy.h"

class TTiPrivate : public DTSerialProxy {
 public:
  ViUInt32 job;
  ViUInt32 rcount;

};

#define job port->job
#define rcount port->rcount


TTi::TTi(char* resName) : softLimitsSet(false)
{
  initSupply(resName, -1, (char*)"UNKNOWN");

  vMaxSoft = vMax;
  iMaxSoft = iMax;
  softLimitsSet = true;
}

TTi::TTi(char* resName, int channel, char* label)
  : softLimitsSet(false)
{
  initSupply(resName, channel, label); 

  vMaxSoft = vMax;
  iMaxSoft = iMax;
  softLimitsSet = true;
}

TTi::TTi(char* resName, int channel, char* label, float maxVoltage, float maxCurrent)
  : vMaxSoft(maxVoltage), iMaxSoft(maxCurrent), softLimitsSet(true)
{
  initSupply(resName, channel, label);
}

TTi::TTi(unsigned int gpib)
  : softLimitsSet(false)
{
  sprintf(stringinput,"GPIB::%d::INSTR",gpib);
  initSupply(stringinput, -1, (char*)"UNKNOWN");

  vMaxSoft = vMax;
  iMaxSoft = iMax;
  softLimitsSet = true;
}

TTi::TTi(unsigned int gpib, int channel, char* label)
  : softLimitsSet(false)
{
  sprintf(stringinput,"GPIB::%d::INSTR",gpib);
  initSupply(stringinput, channel, label);
  
  vMaxSoft = vMax;
  iMaxSoft = iMax;
  softLimitsSet = true;
}

TTi::TTi(unsigned int gpib, int channel, char* label, float maxVoltage, float maxCurrent)
  : vMaxSoft(maxVoltage), iMaxSoft(maxCurrent), softLimitsSet(true)
{
  sprintf(stringinput,"GPIB::%d::INSTR",gpib);
  initSupply(stringinput, channel, label);
}
void TTi::initSupply(char* resName, int channel, char* label)
{
  port = new TTiPrivate;
  
  strcpy(m_resource, resName);
  strcpy(model,"UNKNOWN");
  m_outChannel = -1;  //initializing values 
  m_deviceType = -1;
  debug = false;
  
  // Open session to VISA resource manager
  ViStatus status = port->viOpenDefaultRM ();
  
  if (status < VI_SUCCESS)
    {
      std::cout << "TTi::TTi Could not open a session to the VISA Resource Manager :-(" << std::endl;
      exit (EXIT_FAILURE);
    }
  else {std::cout<<"Opened a session to VISA Resource Manager! :)"<<std::endl;}

  //// Open specified resource //???
  if(strncmp(m_resource,"ASRL",4)==0){
    status = port->viOpen (m_resource, 4, VI_NULL);
    std::cout<<"Opened!"<<std::endl;
  }
  else{
    status = port->viOpen (m_resource, VI_NULL, VI_NULL);
    std::cout<<"Opened w/ LINUX"<<std::endl;
  }
 
  
  // Query device identity
  if(strncmp(m_resource, "/dev", 4) == 0) {
    // Direct linux impl -> CRLF
    sprintf(stringinput,"*IDN?\r\x0A");// ";" deleted PWP 16032018
    std::cout<<"Direct linux"<<std::endl;
  } else {
    // Going via VISA (either Windows or Linux) interposes a correction,
    //  so leave it as it was for now
    sprintf(stringinput,"*IDN?\x0A"); // ";" deleted PWP 16032018
    std::cout<<"VISA"<<std::endl;
  }

  ViUInt32 BytesToWrite = strlen(stringinput);
  status = port->viWrite ((ViBuf)stringinput,BytesToWrite, &rcount);
  
  status = port->viRead(data, 1024, &job);
  
  if(debug) std::cout << "CMD: "<< stringinput << " RSP: " << data << std::endl;
  
  ////////////////////
  
  char *token; 
  
  token = strtok((char*)data,","); 
  std::cout<<"data = "<<data<<std::endl;
  
  token = strtok(NULL,","); //to stop strktok 
  
  if (debug) {std::cout << "token" << token << std::endl;}
  
  // Strip leading space (if present)
  if(token[0]==' ') token ++;
  
  if (strncmp(token, "PST-3202", 8) == 0){
    // GW INSTEK PST-3202GP triple output power supply
    strcpy(model, label);
    if (channel == 3){
      vMax = 6;
      iMax = 5;
    }
    else{
      vMax = 32;
      iMax = 2;
    }
    
    isValid = TRUE;
    if ((channel>0) && (channel<4)){
      m_outChannel = channel;
    }
    else{
      m_outChannel = 1; // Assume channel 1 (LH) if not specified
    }
    token = strtok(NULL, ", ");
    strcpy(sn, token);
    m_deviceType = LV_GW_PST3202GP;
    
  }
  //if (isValid) {PrintStatus();}
  return;
}

bool TTi::Off(){
  char stringinputCheck[128];
  ViUInt32 BytesToQuery;

  // Turn PSU OFF
  switch (m_deviceType){
    case LV_GW_PST3202GP:
      // only global ON/OFF is possible :-(
      sprintf(stringinput, ":OUTPUT:STATE 0\n");
      //sprintf(stringinput, ":OUTPUT:STATE ?");
      break;
  }
  ViUInt32 BytesToWrite = strlen(stringinput);
  BytesToQuery = strlen(stringinputCheck);
  memset(data,0,1024*sizeof(unsigned char));

  // Send command  
  ViStatus status = port->viWrite ((ViBuf)stringinput,BytesToWrite, &rcount);

  return true;
}

bool TTi::On()
{
  char stringinputCheck[128];  
  ViUInt32 BytesToQuery;
  
  switch (m_deviceType)
    {
    case LV_GW_PST3202GP:
      // only global ON/OFF is possible :-(
      sprintf(stringinput, ":OUTPUT:STATE 1\n");  
      //sprintf(stringinput, ":OUTPUT:STATE ?");
      break;
    }
  
  ViUInt32 BytesToWrite = strlen(stringinput);
  std::cout<<"stringinput "<<stringinput<<std::endl;

  BytesToQuery = strlen(stringinputCheck);
  std::cout<<"Bytestowrite = "<<BytesToWrite<<std::endl;

  memset(data,0,1024*sizeof(unsigned char));
  
  // Send command
  ViStatus status = port->viWrite ((ViBuf)stringinput, BytesToWrite, &rcount);
  
  std::cout<<"status = "<<status<<"\n";   

  // Query status //Can't work because no value of Bytestoquery is set!
  if(BytesToQuery){
    status = port->viWrite ((ViBuf)stringinputCheck, BytesToQuery, &rcount);
    
    //if (true == CheckError("TTi::On (OP? viWrite)", status)) return false;
    status = port->viRead (data, 1024, &job);
    //if (true == CheckError("TTi::On (OP? viRead)", status)) return false;
    if(debug) std::cout << "CMD: "<< stringinputCheck << " RSP: " << data << std::endl;  
    if(data[0] == '0') {
      std::cout << "TTi::On did not turn ON " << model << " at resource " << m_resource << std::endl;
      return false;
    }
  }

  //std::cout<<"Hi"<<std::endl;
  return true;
}

 
TTi::~TTi(){
  // Destructor
  // - release vi session handles
  // PWP 04/11/2011

  std::cout << "TTi::~TTi destructor called for " << model << " at resource " << m_resource << std::endl;
  port->viClose();
  delete port;
}



