#include "DTSerialProxy.h"

#ifndef PROXY_HAVE_VISA // If PROXY_HAVE_VISA is defined, all the functions are inline.

#include <sys/ioctl.h>

// Note that timeout here is the timeout for opening the device, not for I/O. It is not implemented.

DTSerialProxy::DTSerialProxy(){}  
DTSerialProxy::~DTSerialProxy(){}  

ViSession DTSerialProxy::viOpen(ViConstRsrc name, ViAccessMode mode, ViUInt32 timeout) 
{
  fd = open(name, O_RDWR | O_NOCTTY);
  
  if (fd < 0) return fd;
  
  FD_ZERO(&fdSet); // clear the file descriptor set
  FD_SET(fd, &fdSet); // add the newly opened file to the set
  
  this->timeout.tv_sec = 0;
  this->timeout.tv_usec = 500000;

  // Set up the TTY settings
  memset(&tty, 0, sizeof(tty)); // initialize
  
  // Read in existing settings, and handle any error
  if (tcgetattr(fd, &tty) != 0) {printf("Error %i from tcgetattr: %s\n", errno, strerror(errno));}

  tty.c_lflag |= ICANON;
  tty.c_lflag &= ~(ECHO | ECHONL | IEXTEN | ISIG);

  // BJG query how to set these (is it device dependent?)
  // tty.c_oflag &=  ~(OCRNL | ONLCR | ONLRET | ONOCR |  OFILL | OLCUC | OPOST);

  tty.c_oflag =0;
  tcsetattr(fd, TCSANOW, &tty);
  
  return 0;
}

ViStatus DTSerialProxy::viWrite(ViBuf buf, ViUInt32 cnt, ViPUInt32 retCnt) 
{
  std::cout << "viWrite " << buf << std::endl;
  return write(fd, buf, cnt);
}

ViStatus DTSerialProxy::viRead(ViBuf buf, ViUInt32 cnt, ViPUInt32 retCnt) 
{
  fd_set fdSet = this->fdSet;
  struct timeval timeout = this->timeout;
  memset(buf, 0, sizeof(buf[0]) * cnt);
  *retCnt = 0;

  while (*retCnt < cnt) 
    {
      switch (select(fd + 1, &fdSet, nullptr, nullptr, &timeout)) 
	{
	case -1:
	  printf("Error %i from select() call in TSerialProxy::viRead: %s\n", errno, strerror(errno));
	  return VI_ERROR_IO;

	case 0:
	  // select() call timed out, i.e. there are no bytes available to be read
	  return VI_ERROR_TMO;
	
	default: // success: there are bytes available to be read
	  *retCnt += read(fd, buf + *retCnt, 1);
	  //std::cout << "viRead hi" << (char*)(buf + *retCnt) << std::endl;
	  if (termcharEnabled && buf[*retCnt - 1] == termchar) return VI_SUCCESS_TERM_CHAR;
	  break;
	}
    }
  //std::cout<<"Viread: HIIIIIIIIIIIIIII"<<buf<<std::endl;

  return VI_SUCCESS_MAX_CNT; // Read the requested number of bytes
}

#endif


