#include "DTSerialProxy.h"
#include "TTi.h"
//#include "TDCSDevice.h"
#include <iostream>

//TTi cl;

int main()
{
  char* psName = "/dev/ttyUSB0";
  char* modName = "PST-3202";

  TTi lc(psName,1,modName); //constructor w/ 2 params and new gives dynamic space allocation to object lc 
  
  //lc.Off();

  lc.On();
  //std::cout<<"port = "<<cl.On()<<std::endl;
  
  return 0;
};


//TTi was an abstarct class because it had virtual functions which are basically functions defined ina  class =0. These functions were defined like this in TDCSDevice.h file and that is why I omitted the file from TTi.h  .. 
